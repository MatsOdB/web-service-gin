package database

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var db *gorm.DB
var err error

type Item struct {
	ID          string  `json:"id"`
	Description *string `json:"description"`
	Location    *string `json:"location"`
	Label       *bool   `json:"label"`
	Available   *bool   `json:"available"`
}

// Connects to the database and returns error if the connection to the database can't be established
func InitialMigration() {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	db.AutoMigrate(&Item{})
}

// Returns all items from the database
func AllItems(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var items []Item
	db.Find(&items)
	c.JSON(http.StatusOK, items)
}

// Returns all items from the database
func SearchItems(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var item Item
	json.NewDecoder(c.Request.Body).Decode(&item)
	var items []Item
	if item.ID != "" {
		db.Where("ID = '" + item.ID + "'").Find(&items)
	} else {
		db.Where("description LIKE '%" + *item.Description + "%'").Find(&items)
	}
	c.JSON(http.StatusOK, items)
}

// Updates description of specific item based on id
func UpdateItemDescription(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var item Item
	json.NewDecoder(c.Request.Body).Decode(&item)
	db.Model(&Item{}).Where("id = ?", item.ID).Update("description", item.Description)
	db.Find(&item)
	c.JSON(http.StatusCreated, item)
}

// Updates location of specific item based on id
func UpdateItemLocation(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var item Item
	json.NewDecoder(c.Request.Body).Decode(&item)
	db.Model(&Item{}).Where("id = ?", item.ID).Update("location", item.Location)
	db.Find(&item)
	c.JSON(http.StatusCreated, item)
}

// Change label status of specific item based on id
func UpdateLabel(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var item Item
	json.NewDecoder(c.Request.Body).Decode(&item)
	// db.Where("id = ?", item.ID)
	// db.Statement.Raw("WHERE id = ?", item.ID, "UPDATE available SET ?", 0)
	db.Model(&Item{}).Where("id = ?", item.ID).Update("label", item.Label)
	db.Find(&item)
	c.JSON(http.StatusCreated, item)
}

// Change availability status of specific item based on id
func UpdateAvailability(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var item Item
	json.NewDecoder(c.Request.Body).Decode(&item)
	// db.Where("id = ?", item.ID)
	// db.Statement.Raw("WHERE id = ?", item.ID, "UPDATE available SET ?", 0)
	db.Model(&Item{}).Where("id = ?", item.ID).Update("available", item.Available)
	db.Find(&item)
	c.JSON(http.StatusCreated, item)
}

// Updates all fields of specific item based on id
func UpdateItem(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var item Item
	json.NewDecoder(c.Request.Body).Decode(&item)
	db.Where("id = ?", item.ID).UpdateColumns(item)
	db.Find(&item)
	c.JSON(http.StatusCreated, item)
}

// Updates all fields of specific items based on id
func UpdateItems(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var items []Item
	json.NewDecoder(c.Request.Body).Decode(&items)
	for i := 0; i < len(items); i++ {
		db.Where("id = ?", items[i].ID).UpdateColumns(items[i])
	}
	c.JSON(http.StatusCreated, items)
}

// Deletes an item based on id
func DeleteItem(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var item Item
	json.NewDecoder(c.Request.Body).Decode(&item)
	db.Where("id = ?", item.ID).Delete(&item)
	c.JSON(http.StatusNoContent, "")
}

// Deletes a list of items based on id
func DeleteItems(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var items []Item
	json.NewDecoder(c.Request.Body).Decode(&items)
	for i := 0; i < len(items); i++ {
		db.Where("id = ?", items[i].ID).Delete(&items[i])
	}
	c.JSON(http.StatusNoContent, "")
}

// Adds an item to the database
func AddItem(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var item Item
	json.NewDecoder(c.Request.Body).Decode(&item)
	db.Create(item)
	c.JSON(http.StatusCreated, item)
}

// Adds a list of items to the database
func AddItems(c *gin.Context) {
	db, err = gorm.Open(sqlite.Open("./database/db/inventory.db"))
	if err != nil {
		fmt.Println(err.Error())
		panic("Failed to connect to the database")
	}

	var items []Item
	json.NewDecoder(c.Request.Body).Decode(&items)
	for i := 0; i < len(items); i++ {
		db.Create(items[i])
	}
	c.JSON(http.StatusCreated, items)
}