package main

import (
	db "bitbucket.org/MatsOdB/web-service-gin/src/master/database"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.GET("/AllItems", db.AllItems)
	r.GET("/SearchItems", db.SearchItems)
	r.PATCH("/UpdateItemDescription", db.UpdateItemDescription)
	r.PATCH("/UpdateItemLocation", db.UpdateItemLocation)
	r.PATCH("/UpdateItemLabel", db.UpdateLabel)
	r.PATCH("/UpdateItemAvailability", db.UpdateAvailability)
	r.PUT("/UpdateItem", db.UpdateItem)
	r.PUT("/UpdateItems", db.UpdateItems)
	r.DELETE("/DeleteItem", db.DeleteItem)
	r.DELETE("/DeleteItems", db.DeleteItems)
	r.POST("/AddItem", db.AddItem)
	r.POST("/AddItems", db.AddItems)

	db.InitialMigration()
	r.Run(":8080")
}
